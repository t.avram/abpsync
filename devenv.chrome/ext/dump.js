"use strict";

const GENERAL_SUBSCRIPTION = '[Subscription]';
const SPECIFIC_SUSBSCRIPTION = '[Subscription filters]';
const SINGLE_FILTER_SECTION = '[Filter]'
const SERVER_URL = "http://1.1.0.1";
const WHITELISTED_WEBSITE_REGEX = /^@@\|\|([^/:]+)\^\$document$/;
const UPLOAD =
{
  url: SERVER_URL + "/upload",
  method: "POST"
};
const DOWNLOAD =
{
  url: SERVER_URL + "/download",
  method: "POST"
}
const GET_ID =
{
  url: SERVER_URL + "/id",
  method: "GET"
}
const SEND_ID_BY_EMAIL =
{
  url: SERVER_URL  + "/email",
  methd: "POST"
}

function uploadSettingsDump()
{
    let rawDump = "";
    browser.storage.local.get().then(
      (settings) => rawDump = settings["file:patterns.ini"].content.join("\n")
    );
    let contentToSend = cleanedUpDump(rawDump);
    localstorage.getItem('myDataKey', id);
    contentToSend.id = id;
    fetch(UPLOAD.url,
      {
        method: UPLOAD.method,
        body: JSON.stringify(contentToSend),
        headers: {
          "Content-Type": "application/json"
        }
      }).then((response) =>
      {
        chrome.runtime.sendMessage({type: "submitSettings", value: "Sent"});
      }, (error) =>
      {
        chrome.runtime.sendMessage({type: "submitSettings", value: "Error"});
      });
}


function getSettings()
{
    // TODO: Implement this.
}


function cleaupDump(rawDump)
{
  const lines = rawDump.split('\n');
  let cleanedUpDump = [];
  let idx = 0;
  let customFilters = [];
  let whitelistedWebsites = [];

  while (idx < lines.length)
  {
    const line = lines[idx];
    if line.includes(GENERAL_SUBSCRIPTION)
    {
      let subscriptionData = Object();
      idx++;
      while (!line[idx].includes(SPECIFIC_SUSBSCRIPTION))
      {
        if (line[idx] != "")
        {
          const splittedUpLine = line[idx].split("=");
          const key = splittedUpLine[0];
          const content = splittedUpLine.slice(1).join("");
          subscriptionData[key] = content;
        }
        idx++;
      }
      if subscriptionData.url.startsWith("~user")
      {
        filters = [];

        // It's a custom filters section. Processing it in a different manner.
        while (
          !line[idx].includes(GENERAL_SUBSCRIPTION) && !line[idx].includes(SINGLE_FILTER_SECTION)
        )
        {
          const filter = line[idx];
          const match = filter.match(WHITELISTED_WEBSITE_REGEX);
          if (match)
          {
            // It's a whitelisted website
            whitelistedWebsites.push({website: match});
            continue;
          }
          // Otherwise, it is a custom filter
          customFilters.push({filter: filter, disabled: false});
        }
      }
      elseif (!Object.entries(subscriptionData).length === 0)
      {
        cleaupDump.push(subscriptionData);
      }
    }
    elseif (line.includes(SINGLE_FILTER_SECTION))
    {
      // Parsing special filter options.
      const filter = lines[idx+1];
      if lines[idx+2].inclues("disabled=true")
        for f in customFilters
          if f.filter == filter f.disabled = true;
      idx+=2;
    }
    else idx++;
  }

  return {
    filterLists: cleanedUpDump,
    customFilters: customFilters,
    whitelistedWebsites: whitelistedWebsites
  };
}
